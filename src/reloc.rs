use std::cell::RefCell;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::io::{BufRead, stdin};

use clap::ArgMatches;

use sorry::TargetProcess;

use crate::error::Error;

/// Representation of a relocation in Callamar. A relocation consists in an original address and
/// an original size that can correspond to an instruction or a whole basic-block (maybe even
/// more) to which are associated a new address and a new size where the code is relocated. The
/// size may have changed if the code has been modified as well so Callamar takes it into account.
#[derive(Debug)]
pub struct Relocation {
    origin_address: u64,
    origin_size: u64,
    target_address: u64,
    target_size: u64
}

impl<'a> TryFrom<&'a str> for Relocation {
    type Error = Error;

    /// A relocation line is made of at least 4 numbers in hexadecimal (without prefix!) that
    /// correspond to (in order): original_address, original_size, target_address, target_size.
    fn try_from(s: &'a str) -> Result<Self, Error> {
        let mut words = s.split_whitespace();
        let count = s.split_whitespace().count();

        if count < 4 {
            Err(Error::CustomError(format!("Wrong format for relocation: {}", s)))
        } else {
            let addr = u64::from_str_radix(words.next().unwrap(), 16)?;
            let size = u64::from_str_radix(words.next().unwrap(), 16)?;
            let target = u64::from_str_radix(words.next().unwrap(), 16)?;
            let target_size = u64::from_str_radix(words.next().unwrap(), 16)?;

            Ok(Relocation {
                origin_address: addr,
                origin_size: size,
                target_address: target,
                target_size: target_size
            })
        }
    }
}

/// The relocation manager maps an address in the original .text section of a process to a
/// relocated address in some other section. This allows Callamar to handle processes that have
/// been modified dynamically and in which parts of the code have been relocated somewhere else.
pub struct RelocationManager {
    text_start: u64,
    text_end: u64,
    relocs: Vec<Relocation>,
    active: bool,

    relocated_memo: RefCell<HashMap<u64, bool>>,
    relocation_memo: RefCell<HashMap<u64, Option<u64>>>
}

impl RelocationManager {
    /// Create a new relocation manager using the given target process. The main .text section of
    /// the binary file corresponding to the process is used by the relocation manager as
    /// boundaries of the accepted addresses.
    pub fn new(target: &TargetProcess) -> Self {
        let text = target
            .get_controller()
            .find_memory_map_entry( |entry| {
                let filename = target.get_executable_name();
                entry.filename.as_ref().map( |f| f.contains(filename) ).unwrap_or(false)
                    && entry.permissions.is_executable()
            }).unwrap();

        RelocationManager {
            text_start: text.start_addr,
            text_end: text.end_addr,
            relocs: Vec::new(),
            active: false,

            relocated_memo: RefCell::new(HashMap::new()),
            relocation_memo: RefCell::new(HashMap::new())
        }
    }

    /// Build a new relocation manager using the program-argument matches. If the user specified
    /// that they want to provide relocations from stdin, the new relocation manager is filled with
    /// the lines read from stdin.
    pub fn from_arg_matches<'a>(target: &TargetProcess, matches: &ArgMatches<'a>) -> Self {
        let mut relocs = RelocationManager::new(&target);

        if matches.is_present("reloc") {
            let stdin = stdin();

            for line in stdin.lock().lines() {
                if let Ok(reloc) = Relocation::try_from(line.unwrap().as_str()) {
                    relocs.add(reloc);
                }
            }

            relocs.active = true;
        }

        relocs
    }

    /// Test if the given address is in the range of addresses where the target .text section is
    /// loaded in the process address space.
    pub fn accepts(&self, address: u64) -> bool {
        self.active && self.text_start <= address && address < self.text_end
    }

    /// Test if the given address corresponds to a relocated block of code.
    pub fn is_relocated(&self, address: u64) -> bool {
        let mut memo = self.relocated_memo.borrow_mut();
        memo.get(&address).map( |bool_ref| *bool_ref )
            .unwrap_or_else( || {
                let res = self.relocs.iter()
                    .find( |reloc| {
                        let start = reloc.target_address;
                        let end = start + reloc.target_size;
                        start <= address && address < end
                    })
                    .is_some();

                memo.insert(address, res);
                res
            })
    }

    /// Add a new relocation to the relocation manager.
    pub fn add(&mut self, reloc: Relocation) {
        self.relocs.push(reloc);
    }

    /// Get the relocated address of the given address.
    pub fn relocated_address(&self, address: u64) -> Option<u64> {
        let mut memo = self.relocation_memo.borrow_mut();
        memo.get(&address)
            .map( |opt_ref| *opt_ref )
            .unwrap_or_else( || {
                let res = self.relocs.iter()
                    .find( |reloc| {
                        let start = reloc.origin_address;
                        let end = start + reloc.origin_size;
                        start <= address && address < end
                    })
                    .map( |reloc| address - reloc.origin_address + reloc.target_address );

                memo.insert(address, res);
                res
            })
    }
}
