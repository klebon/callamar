use std::num::ParseIntError;

use capstone::Error as CsError;

use nix::Error as NixError;

use sorry::error::Error as SorryError;

/// Error-type of the whole Callamar project.
#[derive(Debug, PartialEq)]
pub enum Error {
    /// An error in the capstone library.
    CapstoneError(CsError),
    /// A custom error.
    CustomError(String),
    /// An error in the nix library.
    NixError(NixError),
    /// An error parsing an integer.
    ParseIntError(ParseIntError),
    /// An error in the sorry library.
    SorryError(SorryError)
}

pub type Result<T> = std::result::Result<T, Error>;

impl From<CsError> for Error {
    fn from(err: CsError) -> Self {
        Error::CapstoneError(err)
    }
}

impl From<NixError> for Error {
    fn from(err: NixError) -> Self {
        Error::NixError(err)
    }
}

impl From<ParseIntError> for Error {
    fn from(err: ParseIntError) -> Self {
        Error::ParseIntError(err)
    }
}

impl From<SorryError> for Error {
    fn from(err: SorryError) -> Self {
        Error::SorryError(err)
    }
}
