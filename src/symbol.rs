use std::cell::RefCell;
use std::collections::HashMap;

use elf::types::Symbol;

use sorry::TargetProcess;

use crate::error::Result;
use crate::insn::Insn;
use crate::reloc::RelocationManager;

pub struct SymbolManager {
    relocs: RelocationManager,

    symbol_memo: RefCell<HashMap<u64, Option<(Symbol, u64)>>>
}

/// Results of the memory mapping selection process to identify which symbol may correspond to a
/// given instruction.
struct MemoryMappingResult {
    pub start_addr: u64,
    pub symbols: Vec<Symbol>,
}

/// Find the memory mapping that contains the address of the given instruction. If the instruction
/// is among the ones that have been relocated, the memory mapping of the main ELF object of the
/// program is used instead.
///
/// TODO: change that behavior to make it use original address of the instruction instead so that
/// any code anywhere can be considered relocated.
fn memory_mapping(target: &TargetProcess, relocs: &RelocationManager, insn: &Insn)
-> Option<MemoryMappingResult> {
    let ctrl = target.get_controller();
    let executable = ctrl.get_executable_name();

    let is_relocated = relocs.is_relocated(insn.address);

    ctrl.find_memory_map_entry( |entry| {
        if is_relocated {
            entry.filename.as_ref().map( |f| f.contains(executable) ).unwrap_or(false)
                && entry.permissions.is_executable()
        } else {
            entry.start_addr <= insn.address
                && insn.address < entry.end_addr
                && entry.permissions.is_executable()
        }
    }).and_then( |entry| {
        // The .text section of the memory mapping (if any) is retrieved and the load address of
        // this section in the address space are computed from the retrieved memory mapping.
        let elf = entry.elf.as_ref()?;
        elf.get_section(".text").map( |text| {
            let offset = entry.start_addr - (text.shdr.addr >> 12 << 12);

            MemoryMappingResult {
                start_addr: offset,
                symbols: entry.get_all_symbols(),
            }
        })
    })
}

impl SymbolManager {
    pub fn new(relocs: RelocationManager) -> Self {
        SymbolManager {
            relocs: relocs,

            symbol_memo: RefCell::new(HashMap::new())
        }
    }

    /// Get the symbol which address corresponds to the given instruction.
    pub fn symbol_at(&self, target: &TargetProcess, insn: &Insn) -> Result<Option<(Symbol, u64)>> {
        let mut memo = self.symbol_memo.borrow_mut();

        if let Some(res) = memo.get(&insn.address) {
            Ok(res.clone())
        } else {
            // Find the memory mapping that contains the instruction address.
            let opt = memory_mapping(target, &self.relocs, insn);

            // From the starting address of the right .text section and the symbol list, the symbol
            // corresponding to the input instruction (if any) is retrieved. The instruction must
            // not be at the exact address of the symbol, but it must be inside its boundaries. It
            // enables to compute outputs such as e.g. (main + 0x1c).
            let res = opt.and_then( |mapping| {
                let start_addr = mapping.start_addr;

                // We try to retrieve the right symbol.
                mapping.symbols.into_iter().find_map( |sym| {
                    let sym_start = start_addr + sym.value;
                    let sym_end = sym_start + sym.size;

                    // If there are relocations, we want the symbol's boundaries to be matched
                    // against the new addresses. We retrieve the relocated addresses of the symbol
                    // if any.  Before doing the actual comparison.
                    let (start, end) = if self.relocs.accepts(sym_start) {
                        let start = self.relocs.relocated_address(sym_start).unwrap_or(sym_start);
                        let end = self.relocs.relocated_address(sym_end).unwrap_or(sym_end);
                        (start, end)
                    } else {
                        (sym_start, sym_end)
                    };

                    if start <= insn.address && insn.address < end {
                        Some((sym.clone(), insn.address - start))
                    } else {
                        None
                    }
                })
            });

            memo.insert(insn.address, res.clone());
            Ok(res)
        }
    }
}
