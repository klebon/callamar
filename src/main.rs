extern crate capstone;
extern crate clap;
extern crate elf;
extern crate nix;
extern crate sorry;

mod error;
mod insn;
mod reloc;
mod symbol;

use std::str::FromStr;

use clap::{Arg, ArgMatches, ArgGroup, App};

use elf::types::Symbol;

use nix::sys::signal::Signal;
use nix::sys::wait::WaitStatus;

use sorry::TargetProcess;
use sorry::controller::TargetController;
use sorry::breakpoint::{Breakpoint, Mode};

use crate::error::{Error, Result};
use crate::insn::{CALL_GROUP, Insn, InsnManager};
use crate::reloc::RelocationManager;
use crate::symbol::SymbolManager;

/// Specification of callamar's command line.
fn commandline_arguments<'a, 'b>() -> App<'a, 'b> {
    App::new("callamar")
        .version("0.1")
        .author("Camille Le Bon <camille.le-bon@inria.fr>")
        .about("Trace every call instructions in the process execution")
        .arg(Arg::with_name("file")
            .short("f")
            .long("file")
            .help("ELF executable file to run and trace")
            .takes_value(true))
        .arg(Arg::with_name("pid")
            .short("p")
            .long("pid")
            .help("PID of the process to trace")
            .takes_value(true))
        .arg(Arg::with_name("args")
            .long("args")
            .multiple(true)
            .use_delimiter(false)
            .takes_value(true)
            .allow_hyphen_values(true)
            .requires("file"))
        .arg(Arg::with_name("instruction")
            .short("i")
            .long("instruction")
            .help("Display the jump instruction")
            .takes_value(false))
        .arg(Arg::with_name("jumps")
            .short("j")
            .long("jumps")
            .help("Display every jumps and not only the calls")
            .takes_value(false))
        .arg(Arg::with_name("color")
            .short("c")
            .long("color")
            .help("Color the output")
            .takes_value(false))
        .arg(Arg::with_name("reloc")
            .long("reloc")
            .help("Take relocation information from stdin")
            .takes_value(false))
        .group(ArgGroup::with_name("program")
            .required(true)
            .args(&["pid", "file"]))
}

/// Extract the target program arguments from Damas' command-line.
fn extract_target_arguments<S: AsRef<str>>(prgm: &S, matches: &ArgMatches) -> Vec<String> {
    let mut res = vec![prgm.as_ref().to_string()];

    if let Some(args) = matches.values_of_lossy("args") {
        for arg in args {
            res.push(arg);
        }
    }

    res
}

/// Create a TargetProcess object using the commandline arguments.
fn create_target_process(matches: &ArgMatches) -> Result<TargetProcess> {
    match matches.value_of("pid") {
        Some(pid_str) => {
            let err_msg = format!("Could not parse PID '{}'", pid_str);
            let pid = FromStr::from_str(pid_str).or(Err(Error::CustomError(err_msg)))?;
            TargetProcess::from_pid(pid).or_else( |err| Err(Error::from(err)) )
        }

        None => {
            let filename = matches.value_of("file").unwrap().to_string();
            let args = extract_target_arguments(&filename, &matches);

            // Create the TargetProcess object and start it.
            let target = TargetProcess::new(filename, args);
            target.start().ok_or(Error::CustomError("Could not start".to_string()))?;
            Ok(target)
        }
    }
}

/// Set a breakpoint at the main function's beginning and reach it.
fn reach_main_breakpoint(target: &TargetProcess) -> Result<WaitStatus> {
    let ctrl = target.get_controller();

    let main_offset = target.get_main_function_offset()?;
    let load_addr = target.get_controller().get_absolute_load_address();
    let mut main_breakpoint = Breakpoint::new(load_addr + main_offset, Mode::OneShot);

    main_breakpoint.set(&ctrl)?;
    ctrl.resume(false)?;
    let brk_status = target.wait(None)?;

    main_breakpoint.clean_code(&ctrl)?;
    ctrl.load_map_file()?;

    Ok(brk_status)
}

/// Resume the process execution and wait for a signal. This function is used to step into the
/// process execution and either retrieve the next control-flow instruction or detect its
/// termination.
fn resume_and_wait(ctrl: &TargetController) -> Result<WaitStatus> {
    ctrl.resume(false)?;
    ctrl.wait(None).map_err( |err| err.into() )
}

/// Format the display of the symbol with an optional offset. If the color option is set, the
/// result string will contain ANSI codes to color the output. The add_space parameter determines
/// if an additional space must be added at the end of the string (it is used to have a space
/// between the caller symbol and the -> in the output).
fn format_symbol(sym: Symbol, offset: u64, color: bool, add_space: bool) -> String {
    let epilogue = if add_space { " " } else { "" };
    let color_start = if color { "\x1b[32m" } else { "" };
    let color_end = if color { "\x1b[00m" } else { "" };

    if sym.name.is_empty() {
        "".to_string()
    } else {
        if offset == 0 {
            format!("{}({}){}{}", color_start, sym.name, epilogue, color_end)
        } else {
            format!("{}({} + {:#x}){}{}", color_start, sym.name, offset, epilogue, color_end)
        }
    }
}

fn main() -> Result<()> {
    let matches = commandline_arguments().get_matches();
    let target = create_target_process(&matches)?;

    // If the input program was an ELF file and not a PID, it must at least reach the main function
    // for the C library to be loaded so that Sorry starts being useful.
    if matches.value_of("pid") == None {
        let status = reach_main_breakpoint(&target)?;
        assert_eq!(status, WaitStatus::Stopped(target.get_pid(), Signal::SIGTRAP));
    } else {
        let ctrl = target.get_controller();
        ctrl.load_map_file()?;
        ctrl.calculate_load_address();
        let status = ctrl.wait(None)?;
        assert_eq!(status, WaitStatus::Stopped(target.get_pid(), Signal::SIGSTOP));
    }

    let ctrl = target.get_controller();
    let pc = ctrl.get_registers()?.rip;
    let mut brk = Breakpoint::new(pc, Mode::OneShot);
    brk.set(&ctrl)?;

    let show_instructions = matches.is_present("instruction");
    let show_colors = matches.is_present("color");
    let show_jumps = matches.is_present("jumps");

    ctrl.load_map_file()?;
    let relocs = RelocationManager::from_arg_matches(&target, &matches);
    let symbol_mngr = SymbolManager::new(relocs);
    let insn_mngr = InsnManager::new();

    loop {
        let status = resume_and_wait(&ctrl)?;

        if let WaitStatus::Stopped(_, signal) = status {
            if signal != Signal::SIGTRAP && signal != Signal::SIGSTOP {
                break;
            }

            // Clean the current breakpoint before anything else.
            brk.clean_code(&ctrl)?;

            // Get the current instruction, supposedly the control-flow instruction.
            let call_insn = Insn::at_pc(&target)?;

            // Singlestep in order to get into the jump target.
            ctrl.singlestep_resume()?;
            let status = ctrl.wait(None)?;
            assert_eq!(status, WaitStatus::Stopped(target.get_pid(), Signal::SIGTRAP));

            let dst_insn = Insn::at_pc(&target)?;

            // Print the jump and its target.
            if call_insn.is_a(CALL_GROUP) || show_jumps {
                if show_instructions {
                    print!("{:9} {:14}\t", call_insn.mnemonic, call_insn.op_str);
                }

                let call_sym = symbol_mngr.symbol_at(&target, &call_insn)?
                    .map( |(sym, offset)| format_symbol(sym, offset, show_colors, true) )
                    .unwrap_or("".to_string());

                let dst_sym = symbol_mngr.symbol_at(&target, &dst_insn)?
                    .map( |(sym, offset)| format_symbol(sym, offset, show_colors, false) )
                    .unwrap_or("".to_string());

                let color_start = if show_colors { "\x1b[34m" } else { "" };
                let color_end = if show_colors { "\x1b[00m" } else { "" };

                println!("{}{:#x}{} {}-> {}{:#x}{} {}",
                    color_start, call_insn.address, color_end, call_sym,
                    color_start, dst_insn.address, color_end, dst_sym);
            }

            // As long as there is a next control-flow instruction, a breakpoint is set and this
            // loop continues to execute.
            if let Ok(insn) = insn_mngr.next_cti(&target) {
                brk = Breakpoint::new(insn.address, Mode::OneShot);
                brk.set(&ctrl)?;
            }
        } else {
            break;
        }
    }

    Ok(())
}
