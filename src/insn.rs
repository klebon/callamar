use std::cell::RefCell;
use std::collections::HashMap;
use std::convert::TryFrom;

use capstone::Insn as CsInsn;
use capstone::InsnGroupId;
use capstone::arch::ArchOperand;
use capstone::arch::x86::{X86InsnGroup, X86Operand};

use nix::unistd::Pid;

use sorry::TargetProcess;
use sorry::assembly::create_capstone_object;
use sorry::buffer::Buffer;
use sorry::buffer::remote::RemoteBuffer;

use crate::error::{Error, Result};


pub const CALL_GROUP: InsnGroupId = InsnGroupId(X86InsnGroup::X86_GRP_CALL as u8);
pub const JUMP_GROUP: InsnGroupId = InsnGroupId(X86InsnGroup::X86_GRP_JUMP as u8);
pub const RET_GROUP: InsnGroupId = InsnGroupId(X86InsnGroup::X86_GRP_RET as u8);

const TEMPORARY_BUFFER_SIZE: usize = 50;

#[derive(Clone, Debug, PartialEq)]
pub struct Insn {
    pub address: u64,
    pub size: usize,
    pub bytes: Vec<u8>,
    pub op_str: String,
    pub mnemonic: String,
    pub operands: Vec<X86Operand>,
    pub groups: Vec<InsnGroupId>
}

impl<'a> TryFrom<CsInsn<'a>> for Insn {
    type Error = Error;

    fn try_from(insn: CsInsn<'a>) -> Result<Self> {
        use ArchOperand::*;

        let cs = create_capstone_object();
        let detail = cs.insn_detail(&insn)?;

        let operands = detail.arch_detail().operands().iter().map( |op| {
            match op {
                X86Operand(op) => op.clone(),
                _ => unreachable!()
            }
        }).collect();

        let groups = detail.groups().collect();

        Ok(Insn {
            address: insn.address(),
            size: insn.bytes().len(),
            bytes: insn.bytes().into(),
            op_str: insn.op_str().unwrap().to_string(),
            mnemonic: insn.mnemonic().unwrap().to_string(),
            operands: operands,
            groups: groups
        })
    }
}

impl Insn {
    /// Create an Insn from the instruction at the current PC of the target process.
    pub fn at_pc(target: &TargetProcess) -> Result<Insn> {
        let cs = create_capstone_object();

        let pc = target.get_controller().get_registers()?.rip;
        let remote = RemoteBuffer::new(target.get_pid(), pc, 15);
        let buf = remote.read(15, 0)?;

        let insns = cs.disasm_count(buf.as_ref(), pc, 1)?;
        insns.iter()
            .map( |insn| Insn::try_from(insn) )
            .nth(0)
            .unwrap()
    }

    /// Test if the instruction belongs to the given group.
    pub fn is_a(&self, group: InsnGroupId) -> bool {
        self.groups.iter().any( |g| *g == group)
    }

    /// Test if the instruction is a jump.
    pub fn is_jump(&self) -> bool {
        self.groups.iter().any( |g| *g == CALL_GROUP || *g == JUMP_GROUP || *g == RET_GROUP )
    }
}

/// The instruction manager has only one job: find the next control-flow instruction the target
/// process will encounter. Instead of using a naked function, the manager is needed to improve
/// performance using memoïzation.
pub struct InsnManager {
    cti_memo: RefCell<HashMap<u64, Insn>>
}

impl InsnManager {
    pub fn new() -> Self {
        InsnManager {
            cti_memo: RefCell::new(HashMap::new())
        }
    }

    /// Find the next control-flow instruction from the current program counter.
    pub fn next_cti(&self, target: &TargetProcess) -> Result<Insn> {
        let mut memo = self.cti_memo.borrow_mut();
        let pc = target.get_controller().get_registers()?.rip;
        let pid = target.get_pid();

        match memo.get(&pc) {
            Some(insn) => Ok(insn.clone()),
            None => {
                let res = self.find_next_cti(pid, pc)?;

                memo.insert(pc, res.clone());
                Ok(res)
            }
        }
    }

    fn find_next_cti(&self, pid: Pid, rip: u64) -> Result<Insn> {
        let cs = create_capstone_object();
        let mut pc = rip;

        loop {
            let remote = RemoteBuffer::new(pid, pc, TEMPORARY_BUFFER_SIZE);
            let buf = remote.read(TEMPORARY_BUFFER_SIZE, 0)?;

            let insns = cs.disasm_all(buf.as_ref(), pc)?;

            let code = insns.iter()
                .map( |insn| Insn::try_from(insn) )
                .collect::<Result<Vec<Insn>>>()?;

            if let Some(insn) = code.iter().find( |insn| insn.is_jump() ) {
                return Ok(insn.clone())
            } else {
                // If the disassembled block of code contains no CTI, the address of the
                // last-but-one instruction is used as PC for the next try. The last instruction is
                // not used because it can be a lucky disassembly of a truncated longer instruction
                // - since the buffer does not contain the whole code.
                if let Some(insn) = code.into_iter().rev().nth(1) {
                    pc = insn.address;
                } else {
                    // If the disassembled code has less than two instructions but no CTI, it means
                    // that there will be no valid CTI afterwards.
                    return Err(Error::CustomError("No valid control-flow instruction".to_string()))
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use sorry::assembly::create_capstone_object;

    use super::*;

    #[test]
    fn is_a() {
        let buf = vec![
            0xe8, 0x00, 0x00, 0x00, 0x00,
            0xff, 0x25, 0x05, 0x00, 0x00, 0x00,
            0xff, 0xd0
        ];

        let cs = create_capstone_object();
        let insns_res = cs.disasm_all(&buf, 0);

        assert!(insns_res.is_ok());

        let insns = insns_res.unwrap();

        let got_call: Result<Vec<bool>> = insns
            .iter()
            .map( |insn| Insn::try_from(insn).map( |i| i.is_a(CALL_GROUP) ) )
            .collect();

        assert!(got_call.is_ok());
        assert_eq!(vec![true, false, true], got_call.unwrap());

        let got_jmp: Result<Vec<bool>> = insns
            .iter()
            .map( |insn| Insn::try_from(insn).map( |i| i.is_a(JUMP_GROUP) ) )
            .collect();

        assert!(got_jmp.is_ok());
        assert_eq!(vec![false, true, false], got_jmp.unwrap());
    }
}
