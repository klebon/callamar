Callamar - dynamic call-instruction tracer
==========================================

Callamar is a tool to trace every call instruction executed by a process. It
can both attach to a existing process using its PID or run an ELF executable
itself.

By default, Callamar only prints the call-instruction address, the target
address and the symbols they correspond to.

```
$ callamar -f /bin/dummy
...
0x7f4ced8d36c5 -> 0x7f4ced8df8b0 (__overflow)
0x7f4ced8ded7e -> 0x7f4ced8de900 (_IO_do_write)
0x7f4ced8de914 -> 0x7f4ced8dcb00 (new_do_write)
0x7f4ced8dcb61 -> 0x7f4ced8dd800 (_IO_file_write)
0x7f4ced8dd828 -> 0x7f4ced94cb40 (__write)
...
```

It is however possible to show more information using a few options:

* `-i`: shows the call-instruction
* ̀`-j`: does not limit the tracing to call-instruction and shows every jumps

```
$ callamar -f /bin/dummy -i -j
...
callq   0x7f33e5beac30  0x7f33e5be9e13 -> 0x7f33e5beac30 (_IO_doallocbuf)
je      0x7f33e5beac40  0x7f33e5beac39 -> 0x7f33e5beac40
je      0x7f33e5beac57  0x7f33e5beac4b -> 0x7f33e5beac57
jbe     0x7f33e5beace0  0x7f33e5beac78 -> 0x7f33e5beac7a
callq   *0x68(%rbp)     0x7f33e5beac7d -> 0x7f33e5bdbf40 (_IO_file_doallocate)
js      0x7f33e5bdc018  0x7f33e5bdbf6a -> 0x7f33e5bdbf70
jbe     0x7f33e5bdc090  0x7f33e5bdbf91 -> 0x7f33e5bdbf97
callq   *0x90(%rbx)     0x7f33e5bdbf9d -> 0x7f33e5be87e0 (_IO_file_stat)
...
```
